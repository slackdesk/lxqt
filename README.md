# LXQt

LXQt desktop environment build scripts for Slackware Linux.  

The script 'build_lxqt.sh' was kindly provided by [Qury via the LQ forum](https://www.linuxquestions.org/questions/slackware-14/lxqt-0-15-0-for-slackware64-current-4175673974/#post6117700) and will clone this repo, download the source files and build all the packages.  

**IMPORTANT: Only if you DO NOT have KDE installed must you build the /deps-kde packages.  
  If you are using the build_lxqt.sh script:**  
    # KDE=yes ./build_lxqt.sh  

If you would rather do it all manually, the 'BUILD_ORDER' gives you a general order in which the packages should be built.

## About LXQt:

From the [LXQt](https://lxqt.org/about/) website:  
>LXQt is a lightweight Qt desktop environment.  
>
>It will not get in your way. It will not hang or slow down your system. It is focused on being a classic desktop with a modern look and feel.  
>
>Historically, LXQt is the product of the merge between LXDE-Qt, an initial Qt flavour of LXDE, and Razor-qt, a project aiming to develop a Qt based desktop environment with similar objectives as the current LXQt.  
>
>LXQt was first supposed to become the successor of LXDE one day but as of 09/2016 both desktop environments will keep coexisting for the time being.

### Note about the desktop and icon themes:

The theme and icon theme have been set to the KDE Plasma themes. ie; breeze icons and kde-plasma theme. This was done because the panel icons were not visible with the default LXQt "frost" theme.

### Notes about /extra packages:

* **pavucontrol-qt**  
 This is an "official" LXQt package, but as it was tested it was found that it is just duplicating the stock Slackware pavucontrol. If you would rather run LXQt's Qt version, you should consider removing the Slackware package or else you'll have duplicates. Please note that removing the Slackware pavucontrol package and using only the LXQt pavucontrol-qt package has not been tested.

* **screengrab**  
This is another "official" LXQt package that when installed is a duplicate application since lximage-qt has it's own screenshot application.

# 

Slackware® is a registered trademark of [Patrick Volkerding](http://www.slackware.com/)  
Linux® is a registered trademark of [Linus Torvalds](http://www.linuxmark.org/)
