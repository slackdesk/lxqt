#!/bin/bash

# This build script was kindly provided by Qury via the LQ forum!
# https://www.linuxquestions.org/questions/slackware-14/lxqt-0-15-0-for-slackware64-current-4175673974/#post6117700

# Changes
# 2021.03.27 Removed libdbusmenu-qt5, polkit-qt5. - Skaendo
# 2021.03.28 Moved KDE scripts to seperate directory. - Skaendo
#   Added option to build KDE packages or not, default not. - Skaendo

KDE=${KDE:-no}

git clone https://gitlab.com/slackdesk/lxqt.git
cd $PWD/lxqt

MAKEFLAGS="-j$(expr $(nproc) + 1) "

ln -sf  $PWD/extra/pavucontrol-qt $PWD/core/

if [ "${KDE:-no}" = "yes" ]; then
  DEPSKDE="\
    extra-cmake-modules \
    solid \
    breeze-icons \
    kidletime \
    plasma-wayland-protocols \
    kwayland \
    libkscreen \
    kwindowsystem "

  cd deps-kde
    for d in $DEPSKDE ; do
      cd "$d"
      . ./$d.info
      wget $DOWNLOAD
      ( chmod +x $d.SlackBuild && env MAKEFLAGS=$MAKEFLAGS ./$d.SlackBuild && upgradepkg --install-new /tmp/$d*.t?z) || exit 1
      cd ..
    done
  cd ..
fi

DEPS="\
muparser \
imlib2 \
openbox \
libstatgrab \
libfm-extra \
lxmenu-data \
menu-cache \
libfm "

cd  deps 

for d in $DEPS
do
	cd "$d"
    . ./$d.info
    wget $DOWNLOAD
	( chmod +x $d.SlackBuild && env MAKEFLAGS=$MAKEFLAGS ./$d.SlackBuild && upgradepkg --install-new /tmp/$d*.t?z) || exit 1
	cd ..
done

cd ../core

CMAKE_REPOS=" \
	lxqt-build-tools \
	libqtxdg \
	liblxqt \
	libsysstat \
	libfm-qt \
	lxqt-themes \
	pavucontrol-qt \
	lxqt-about \
	lxqt-admin \
	lxqt-config \
	lxqt-globalkeys \
	lxqt-notificationd \
	lxqt-openssh-askpass \
	lxqt-policykit \
	lxqt-powermanagement \
	lxqt-qtplugin \
	lxqt-session \
	lxqt-sudo \
	pcmanfm-qt \
	lxqt-panel \
	lxqt-runner \
    lxqt-wallet \
	lxqt-archiver \
    obconf-qt \
	lximage-qt \
	qtermwidget \
	qterminal \
    qps \
    xdg-desktop-portal-lxqt \
    "

for d in $CMAKE_REPOS
do
	cd "$d"
    . ./$d.info
    wget $DOWNLOAD
	( chmod +x $d.SlackBuild && env MAKEFLAGS="$MAKEFLAGS" ./$d.SlackBuild && upgradepkg --install-new /tmp/$d*.t?z) || exit 1
	cd ..
done

cd ../extra

OPTIONAL_CMAKE_REPOS=" \
	screengrab"

for d in $OPTIIONAL_CMAKE_REPOS
do
	cd "$d"
    . ./$d.info
    wget $DOWNLOAD
	( chmod +x $d.SlackBuild && env MAKEFLAGS=$MAKEFLAGS ./$d.SlackBuild && upgradepkg --install-new /tmp/$d*.t?z) || exit 1
	cd ..
done
